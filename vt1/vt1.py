#!/usr/bin/python
# -*- coding: utf-8 -*-

########
# TODO #
########
# 
### TASO 1
#
# [X] Ohjelma osaa ladata osoitteessa http://appro.mit.jyu.fi/tiea2080/vt/vt1/data.json määritellyn json-tietorakenteen ja muuntaa sen pythonin tietorakenteeksi. 
# Voit tutkia tietorakenteen sisältöä osoitteessa http://jsoneditoronline.org/?url=http%3A%2F%2Fappro.mit.jyu.fi%2Ftiea2080%2Fvt%2Fvt1%2Fdata.json
#
# [X] Kirjoita funktio, joka tulostaa tietorakenteen sisältämien kilpailujen ja niiden sisältämien joukkueiden nimet. 
# Kilpailujen nimet on tulostettava kukin omalle rivilleen. 
# Kunkin joukkueen nimi on tulostettava omalle rivilleen neljän välilyönnin verran sisennettynä. Älä tulosta mitään ylimääräistä. Funktion on toimittava riippumatta tietorakenteessa olevien kilpailujen ja joukkueiden määrästä.
# 
# [X] Kirjoita funktio, joka osaa lisätä data-tietorakenteeseen funktiolle parametrina tuodun joukkueen halutun kilpailun pyydettyyn sarjaan. Esim. 
# --> lisaaJoukkue(joukkue, 5372934059196416, "4h"). Jos parametrina annettua kilpailua (sama kilpailun id) tai sarjaa (sama nimi) ei löydy niin funktio ei tee mitään. 
# Pitää toimia millä tahansa sarjoilla eikä vain niillä, jotka löytyvät nyt annetusta rakenteesta
# 
# [X] Lisää edellä luomallasi funktiolla data-tietorakenteeseen yksi uusi joukkue Jäärogainingin 4h-sarjaan seuraavilla tiedoilla:
# {
#       "nimi": u"Mallijoukkue",
#       "jasenet": [
#         u"Tommi Lahtonen",
#         u"Matti Meikäläinen"
#       ],
#       "seura": None,
#       "id": 99999
# }
#
# [X] Ohjelma tulostaa kaikkien joukkueiden nimet. Mukana on oltava myös itse lisätty joukkue
# 
# [X] Kirjoita funktio, joka tulostaa kaikkien kokonaisluvulla alkavien rastien koodit. Esim. jos rastin koodi on LÄHTÖ niin kyseistä koodia ei tulosteta mutta jos rastin koodi on 9D niin tämä koodi tulostetaan. 
# Tulosta kaikki rastikoodit yhdelle riville puolipisteellä (;) eroteltuina. Listauksessa EI saa olla ylim. välilyöntejä. Esim. 3A;50;51;53;5B;7A;99
# . Kts. the print statement.
# 
# [X] Tulosta kokonaisluvulla alkavien rastien koodit joukkuelistauksen jälkeen
# 
# [ ] Tee ohjelmastasi CGI-ohjelma, joka toimii osoitteessa http://users.jyu.fi/~omatunnus/cgi-bin/tiea2080/vt1/vt1.cgi. 
# Ohjelman on tuotettava selaimeen samanlainen tulostus kuin komentoriviltäkin suoritettaessa. Käytä text/plain-tyyppiä.
#
### TASO 3
#
# [ ] Lisää ohjelmaan funktio, joka osaa poistaa annetusta kilpailusta nimen perusteella joukkueen. Funktio ottaa parametrina sarjan ja poistettavan joukkueen nimen. 
# Esim. poistaJoukkue( sarja, "Vara 1");. Huom. parametrina annettava sarja tarkoittaa osaa tietorakenteesta eikä sarjan nimeä.
# 
# [ ] Poista funktiollasi joukkueet joiden nimet ovat: "Vara 1", "Vara 2" ja "Vapaat".
#
# [ ] Ohjelma tulostaa joukkueiden nimet ja pisteet pisteiden mukaisessa laskevassa suuruusjärjestyksessä. Ts. Eniten pisteitä saanut joukkue tulostetaan ensimmäisenä. Tulostuksen on näytettävä seuraavanlaiselta:
# 		Joukkue 1 (213 p)
# 		Joukkue 13 (200 p)
# 		Joukkue 8 (199 p)
# 		...
#		Vinkki: sort. Älä kirjoita omaa järjestysalgoritmia!
#
# [ ] Pisteet lasketaan seuraavalla tavalla:
# 		data["tupa"]-tietorakenteessa on listattu kaikki joukkueiden käymät rastit. Rakenteesta löytyy aika jolloin rastilla on käyty ja rastin sekä joukkueen tunniste.
# 		tupa-rakenne ei ole "tasalaatuista" eli siinä esiintyy rasteja joille ei ole vastinetta rastit-rakenteessa. Samoin esiintyy joukkueita joille ei ole vastinetta. Kaikki nämä pitää osata käsitellä jotenkin järkevästi eli vähintään hypätä yli eikä ohjelma saa kaatua tai laskenta sekoilla.
# 		Käy läpi tupa-tietorakenne ja etsi kunkin joukkueen käymät rastit
# 		Joukkue saa kustakin rastista pisteitä rastin koodin ensimmäisen merkin verran. Esim. jos rastin koodi on 9A niin joukkue saa yhdeksän (9) pistettä. Jos rastin koodin ensimmäinen merkki ei ole kokonaisluku niin kyseisestä rastista saa nolla (0) pistettä. Esim. rasteista LÄHTÖ ja F saa 0 pistettä.
# 		Esim. Dynamic Duon oikea pistemäärä on 206.
#
### TASO 5
#
# [ ] Tulosta joukkueiden nimet, pisteet, kuljettu matka ja käyttämä aika laskevassa suuruusjärjestyksessä pisteiden mukaan eli eniten pisteitä saanut joukkue ensimmäisenä.
# 		Joukkue 1, 213 p, 55 km, 07:50:30
# 		Joukkue 13, 200 p, 50 km, 07:58:00
# 		Joukkue 8, 199 p, 52 km, 07:30:00
# 		Joukkue 11, 199 p, 50 km, 07:48:13
# 		...
# 		Tasapisteissä vähemmän aikaa käyttänyt joukkue tulostetaan ensin. Huom. ohjelmasi pitää tulostaa myös aiempien tasojen vaatimat tulostukset.
#  
# [ ] Laske kuinka pitkän matkan kukin joukkue on kulkenut eli laske kunkin rastivälin pituus ja laske yhteen kunkin joukkueen kulkemat rastivälit. Jos rastille ei löydy sijaintitietoa (lat ja lon) niin kyseistä rastia ei lasketa matkaan mukaan. 
# Voit käyttää apunasi alla annettuja funktioita. Vinkki: Getting distance between two points based on latitude/longitude.
# 
# [ ] Laske joukkueen käyttämä aika eli vähennä joukkueen viimeisestä rastileimausajasta ensimmäisen leimauksen aika. Tulosta aika muodossa: tunnit:minuutit:sekunnit. Vinkki: timedelta
#


###########
# IMPORTS #
###########

import os
import datetime
import urllib2
import time
import argparse
import sys
import simplejson as json

#########################
#   GLOBAL VARIABLES    #
#########################

# Getting the json data to a global variable for handeling it trough the program
JSONDATA = json.load(urllib2.urlopen('http://appro.mit.jyu.fi/tiea2080/vt/vt1/data.json'))

# These are the running parameters, that help developing and debugging
running_parameters = {
    'verbose':      False,
    'message':      "",
}

####################
# ARGUMENT PARSERS #
####################

# prints if verbosity is set on
def myPrint(msg):
    if running_parameters['verbose'] == True:
        print(msg)
    return

#prints the running parameters
def printParameters():
    if(running_parameters['verbose']):
        print("-------------------------------")
        print("RUNNING PARAMETERS:")
        for key, value in running_parameters.items():
            print(key.upper() + ": \t\t" + str(value))
        print("-------------------------------")

# parses the arguments
def parseArguments(argv):
    parser = argparse.ArgumentParser()

    # stores verbosity
    parser.add_argument("-v","--verbose",help="sets verbosity level", action="store_true")

    # stores message
    parser.add_argument("-m","--message",help="filename to be saved", type=str, action="store")

    # parses arguments
    args = parser.parse_args()

    # parses message argument
    if(args.message):
        running_parameters["message"] = str(args.message)

    # parses verbosity argument
    if(args.verbose):
        running_parameters["verbose"] = True


###########################
# ACTUAL COURSE FUNCTIONS #
###########################

#prints out all the competitions and teams
def printCompetitionsAndTeams():
	#gets in to the json data and prints out all the competition names
	for x in range(0,len(JSONDATA)):
		print(JSONDATA[x]["nimi"])
		#starts the second loop goes trough all the series
		try:
			# if there is now series in the praticular competetiton then this will raise an error an continue in the loop
			JSONDATA[x]["sarjat"]
			# starts to loop through the series and uses i as the index that keeps track where we are going
			for i in range(0,len(JSONDATA[x]["sarjat"])):
				#starts the loop for going trough teams in praticular sarja and uses j as a the index to keep track where we are going
				for j in range(0,len(JSONDATA[x]["sarjat"][i]["joukkueet"])):
					# prints out the team name with the indentation of four spaces or one tab
					print("\t%s" % JSONDATA[x]["sarjat"][i]["joukkueet"][j]["nimi"])
		# if there is no sarja in the competition then an exception will be raised and this is executed
		except:
			# continues the loop if there is something to be looped
			continue

# adds a team to the json data  		
def lisaaJoukkue(team, competitionId, seriesToBeAdded):
	#loops through different 
	for x in range(0,len(JSONDATA)):
		#gets jsons part data[x]
		competition = JSONDATA[x]
		# gets the id of the competition that is being handeled
		id = competition["id"]
		# tests if the ID is the same as we are looking for in the parameters
		if(id == competitionId):
			#looping trough all the series in the selected competition
			for s in range(0,len(competition["sarjat"])):
				#gets the series for testing if we have found the series we want to add the team
				series = competition["sarjat"][s]
				# gets the series name so that we can stay in track while going trough the json
				seriesName = series["nimi"]
				# tests if the series name is the one that we want to add the team to
				if(seriesName == seriesToBeAdded):
					# lets get the teams in this praticular series
					teams = series["joukkueet"]
					# we have met all the conditions so lets add the team here
					teams.append(team)

def poistaJoukkue(sarja, teamToBeDeleted):
	try:
		for x in range(0,len(sarja["joukkueet"])):
			joukkue = sarja["joukkueet"][x]
			if(joukkue["nimi"] == teamToBeDeleted):
				sarja["joukkueet"][x].remove(teamToBeDeleted)
				#sarja["joukkueet"][x].remove(#)
				#print("yes")
				#sarja["joukkueet"]joukkue.remove()
	except:
		pass


# prints all the rastit that start with a number
def printRastitThatStartWithWholeNumber():
	# list that is used for the printing
	rastikoodit = []
	# loops through competitions
	for x in range(0,len(JSONDATA)):
		#gets jsons part data[x]
		competition = JSONDATA[x]
		# gets all the rastit
		try:
			# start to work trough the 
			rastit = competition["rastit"]
			#starts to loop trough rastit in the competition
			for rasti in rastit:
				# lets get the first translate the rasti to a string so that we can get the first char
				firstDigit = str(rasti["koodi"])
				# get the first car of the digit
				firstDigit = firstDigit[0:1]
				try:
					# cast the first digit in to the integer to find out if the rasticode is a number if it is not the exception is raised
					firstDigit = int(firstDigit)
					# minor bubblecum and adding the rasticode that was found to the list
					rastikoodit.append(rasti["koodi"])
					# 	I tried here to print he codes like the following
					# print("%s;" % rasti["koodi"]),
					# 	But ended up getting result like the following: 
					# 	66; 6D; 91; 48; 31; 85; 69; 99; 60; 63; 70; 90; 34; 37; 5C; 44; 
					#	I Tried to rasti["koodi"].strip() it out but that did not seem to work
					#	for tha praticular reason I ended up putting some "bubblecum" to the code
					# 	and added the rasticodes to a list and then looped trough the list at the 
					# 	of the function.
				except:
					# if the rasti koodi is not a number then the code jumps here and we will continue in the loop
					continue	
		except:
			# if there are no rastis in the competition, then we come here and the loop continues
			continue
	# initialize the string where the rasticode strings are built
	stringToBeBuilt = ""
	for c in rastikoodit:
		# add the code to the string
		stringToBeBuilt += "%s;" % c
	# print the string
	print stringToBeBuilt

#################
# MAIN PROGRAMM #
#################

if __name__ == "__main__":
    # PARSE ARGUMENTS STARTS
    args = parseArguments(sys.argv)
    printParameters()
    # ARGUMENT PARSE ENDS
    printCompetitionsAndTeams()
    print("")
    joukkue = {
      "nimi": u"Mallijoukkue",
      "jasenet": [
        u"Tommi Lahtonen",
        u"Matti Meikäläinen"
      ],
      "seura": None,
      "id": 99999
	}
    print("")
    lisaaJoukkue(joukkue, 5372934059196416, "4h")
    printCompetitionsAndTeams()
    print("")
    printRastitThatStartWithWholeNumber()
    #poistaJoukkue(JSONDATA[0]["sarjat"][2], "Vara 1")
    #EXIT
    sys.exit(0)