import unittest
import base

#########################
#UNIT TESTS DESCRIPTIONS#
#########################

# Documentation can be found from:
# https://docs.python.org/2/library/unittest.html

# Method							Checks that	
# assertAlmostEqual(a, b)			round(a-b, 7) == 0	 
# assertNotAlmostEqual(a, b)		round(a-b, 7) != 0	 
# assertGreater(a, b)				a > b					
# assertGreaterEqual(a, b)			a >= b					
# assertLess(a, b)					a < b	
# assertLessEqual(a, b)				a <= b
# assertRegexpMatches(s, r)			r.search(s)
# assertNotRegexpMatches(s, r)		not r.search(s)
# assertItemsEqual(a, b)			sorted(a) == sorted(b) and works with unhashable objs
# assertDictContainsSubset(a, b)	all the key/value pairs in a exist in b


#########################
#    TESTS ARE HERE     #
#########################

# Tests are written here
class TestBase(unittest.TestCase):

	# must always be named as test_function_name
	def test_WHAT_FUNCTION_IS_TESTED(self):
		#self.assertEqual(base.FUNCTION(PARAMS),return result)

#########################
#     MAIN PROGRAMM     #
#########################

if __name__ == "__main__":
    unittest.main()
